# $Id$
#
# This Makefile contains all instructions to create RPM and DEB packages
# for the AuditConsole
#
#
#
VERSION=0.6.0
REVISION=0
NAME=auditconsole-server
BUILD=.build_tmp
DEB_FILE=auditconsole-server-${VERSION}-${REVISION}.deb
RPM_FILE=${NAME}-${VERSION}-${REVISION}.noarch.rpm
RPM_DIST=jwall-devel
DEB_DIST=jwall
RELEASE_DIR=releases
RPMBUILD=$(PWD)/RPMBUILD
ARCH=noarch
GPG_KEY_ID=C5C3953C
RPM_RELEASE_FILE=${RPMBUILD}/RPMS/${ARCH}/${RPM_FILE}


all:  deb


pre-package:
	echo "Preparing package build in ${BUILD}"
	mkdir -p ${BUILD}
	mkdir -p ${RELEASE_DIR}
	cp -a dist/opt ${BUILD}/
	cp -a dist/etc ${BUILD}
	


rpm-init:
	mkdir -p ${RPMBUILD}
	mkdir -p ${RPMBUILD}/tmp
	mkdir -p ${RPMBUILD}/BUILD
	mkdir -p ${RPMBUILD}/RPMS
	mkdir -p ${RPMBUILD}/RPMS/${ARCH}
	mkdir -p ${RPMBUILD}/SRPMS
	rm -rf ${RPMBUILD}/${NAME}
	mkdir -p ${RPMBUILD}/${NAME}
	cp -a dist/opt ${RPMBUILD}/${NAME}
	cp -a dist/etc ${RPMBUILD}/${NAME}
	cp -a dist-rpm/etc ${RPMBUILD}/${NAME}
	mkdir -p ${RPMBUILD}/SPECS
	cp -a dist-rpm/auditconsole-server.spec ${RPMBUILD}/SPECS
	

rpm:	rpm-init pre-package
	rm -rf ${BUILD}/DEBIAN
	find ${BUILD} -type f | sed -e s/^\.build_tmp// | grep -v DEBIAN > ${RPMBUILD}/tmp/rpmfiles.list
	rpmbuild --sign --define '_topdir ${RPMBUILD}' --define '_version ${VERSION}' --define '_revision ${REVISION}' -bb ${RPMBUILD}/SPECS/auditconsole-server.spec --buildroot ${RPMBUILD}/auditconsole-server
	cp ${RPM_RELEASE_FILE} ${RELEASE_DIR}


release-rpm:
	mkdir -p /var/www/download.jwall.org/htdocs/yum/${RPM_DIST}/${ARCH}
	cp ${RPM_RELEASE_FILE} /var/www/download.jwall.org/htdocs/yum/${RPM_DIST}/${ARCH}/
	createrepo /var/www/download.jwall.org/htdocs/yum/${RPM_DIST}/


deb:	pre-package
	mkdir -p ${BUILD}/DEBIAN
	cp dist-deb/DEBIAN/* ${BUILD}/DEBIAN/
	cp -a dist/etc ${BUILD}
	cat dist-deb/DEBIAN/control | sed -e "s/Version.*/Version: ${VERSION}-${REVISION}/" > ${BUILD}/DEBIAN/control
	chmod 755 ${BUILD}/DEBIAN/p*
	cd ${BUILD} && find opt -type f -exec md5sum {} \; > DEBIAN/md5sums && cd ..
	cd ${BUILD} && find etc -type f -exec md5sum {} \; >> DEBIAN/md5sums && cd ..
	dpkg -b ${BUILD} ${RELEASE_DIR}/${DEB_FILE}
	debsigs --sign=origin --default-key=${GPG_KEY_ID} ${RELEASE_DIR}/${DEB_FILE}

release-deb:
	reprepro --ask-passphrase -b /var/www/download.jwall.org/htdocs/debian includedeb ${DEB_DIST} ${RELEASE_DIR}/${DEB_FILE}


unrelease-deb:
	reprepro --ask-passphrase -b /var/www/download.jwall.org/htdocs/debian remove ${DEB_DIST} auditconsole-server




clean:	
	rm -rf ${BUILD}
	rm -rf ${RPMBUILD} 
	rm -rf ${RELEASE_DIR}
