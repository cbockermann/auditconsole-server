AuditConsole-Server
===================

This project provides the required files for creating a RPM and Debian
package of an Apache Tomcat servlet container. The container is installed
into `/opt/AuditConsole`.

It is intended to serve as the basic container for running the AuditConsole
web-application.


Building the RPM/Debian
-----------------------

The packages can be build using the Makefile provided. The provide
targets `deb` and `rpm` to create the packages for Debian and RedHat
based systems likewise:

      # make rpm
or

      # make deb

The resulting packages will be created in the `releases` directory.

By default the packages will be signed with a GPG-key. For this to
work, you will need to specify the key-ID in the Makefile.


Layout of the Project
---------------------

The repository mainly consists of the `dist` directory, which includes
the basic files that are to be added to the package.

Additional files for the Debian and RPM packages are located in the
`dist-deb` and `dist-rpm` directories.
