#!/bin/sh
#
# update.sh is a wrapper script to call the jwall-tools console updater
# in order to update a standalone AuditConsole to the latest release.
#


#
# Determine full script location of the update.sh script...
#
PWD=`pwd`
fullpath=`echo "$PWD/$0"`
fullpath_length=`echo ${#fullpath}`
scriptname="$(basename $0)"
scriptname_length=`echo ${#scriptname}`
result_length=`echo $fullpath_length - $scriptname_length - 1 | bc`
SCRIPT_PATH=`echo $fullpath | head -c $result_length`
SCRIPT=$0


#
# Check for a recent jwall-tools.jar 
#
JWALL_TOOLS=/opt/modsecurity/lib/jwall-tools.jar
if [ ! -f $JWALL_TOOLS ]; then
    echo "Using jwall-tools.jar bundled with update script..."
    JWALL_TOOLS=$SCRIPT_PATH/jwall-tools.jar
fi
echo "Using jwall-tools.jar from $JWALL_TOOLS"

JAVA=`which java`
if [ ! -x $JAVA ]; then
    echo "Cannot find an appropriate Java VM."
    exit -1;
fi


JAVA_OPTS=""


#
# If there is no global CONSOLE_HOME set, we assume that
# the update script resides within CONSOLE_HOME/bin and 
# deduct the CONSOLE_HOME directory from that
#
if [ -z $CONSOLE_HOME ]; then
    CONSOLE_HOME="$SCRIPT_PATH/.."
    CONSOLE_HOME=`cd $SCRIPT_PATH/..; pwd`
    echo "Assuming CONSOLE_HOME = $CONSOLE_HOME"
fi

#
# Execute the jwall-tools console-update command on CONSOLE_HOME
#
EXEC="$JAVA -jar $JWALL_TOOLS console-update $CONSOLE_HOME"
$EXEC