Name:		auditconsole-server
Version:	%_version
Release:	%_revision
Summary:	The AuditConsole server is a bundled tomcat application server. 
Group:		admin
URL:		http://www.jwall.org/AuditConsole
Source0:	http://download.jwall.org/AuditConsole/current/
BuildRoot:	%_topdir/auditconsole-server
BuildArch:	noarch
Requires:	java >= 1.7
License:	None

Group: Applications/System


%description
Brief description of software package.

%prep

%build

%install

%clean


%pre
/usr/bin/getent group jwall || /usr/sbin/groupadd -r jwall
/usr/bin/getent passwd jwall || /usr/sbin/useradd -r -d /opt/AuditConsole -s /bin/false jwall

if [ -f /opt/AuditConsole/var/data/.lock ]; then
   echo "Shutting down AuditConsole server..."
   /etc/init.d/auditconsole stop
   sleep 10
fi

%post
chmod 755 /etc/init.d/auditconsole
mkdir -p /opt/AuditConsole/var/tomcat/temp

#if [ -x /etc/init.d/auditconsole ]; then
#   #echo "Starting AuditConsole server..."
#   /etc/init.d/auditconsole start
#fi

%preun
if [ -f /opt/AuditConsole/var/data/.lock ]; then
   #echo "Shutting down AuditConsole server..."
   /etc/init.d/auditconsole stop
   sleep 10
fi

%files -f ../tmp/rpmfiles.list
%defattr(-,root,root)
%doc

%changelog
